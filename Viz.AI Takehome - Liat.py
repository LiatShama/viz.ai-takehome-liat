import urllib
import tarfile
import os
import dicom


def download_tar_file(url, tar_file="tar_file.gz"):
    print "Downloading tar file"
    url_opener = urllib.URLopener()
    url_opener.retrieve(url, tar_file)


def extract_files_from_tar_file(tar_file="tar_file.gz"):
    print "Extracting dicom files from tar file"
    tar = tarfile.open(tar_file)
    tar.extractall()
    tar.close()


def add_patient_data(plan, patients):
    if plan.PatientName not in patients:
        patients[plan.PatientName] = {"Age": [], "Sex": []}
        patients[plan.PatientName]["Age"].append(plan.PatientAge)
        patients[plan.PatientName]["Sex"].append(plan.PatientSex)

    return patients


def create_dicom_file_path(plan):
    return os.path.join(plan.PatientName,
                        plan.StudyInstanceUID,
                        plan.SeriesInstanceUID)


def move_file_to_path(a_file, path):
    if not os.path.exists(path):
        os.makedirs(path)

    os.rename(a_file, os.path.join(path, a_file))


def arrange_dicom_files(dir_path="."):
    patients = {}
    series_duration = {}
    hospitals = []

    print "Arranging dicom files into directories"
    for dcm_file in os.listdir(dir_path):
        if os.path.splitext(dcm_file)[1] == ".dcm":
            plan = dicom.read_file(dcm_file)

            patients = add_patient_data(plan, patients)
            series_duration[plan.SeriesInstanceUID] = float(plan.ExposureTime)
            hospitals.append(plan.InstitutionName)

            dcm_path = create_dicom_file_path(plan)
            move_file_to_path(dcm_file, dcm_path)
    return patients, series_duration, hospitals


def print_patients_data(patients):
    print "\nThere are %d different patients:" % len(patients)
    for patient in patients:
        print patient
        print {key: list(set(patients[patient][key]))
               for key in patients[patient]}


def print_duration_data(series_duration):
    duration_average = sum(series_duration.values()) / len(series_duration)
    print "\nA CT scan take on average %d" % duration_average


def print_hospitals_data(hospitals):
    hospitals = list(set(hospitals))
    print "\nThe data comes from %d different hospitals: " % len(hospitals)
    print hospitals


def main():
    url = raw_input("Enter tar file URL: ")
    download_tar_file(url)
    extract_files_from_tar_file()
    patients, series_duration, hospitals = arrange_dicom_files()
    print_patients_data(patients)
    print_duration_data(series_duration)
    print_hospitals_data(hospitals)


if __name__ == "__main__":
    main()